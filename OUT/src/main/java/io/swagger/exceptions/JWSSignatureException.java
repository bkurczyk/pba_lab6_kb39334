package io.swagger.exceptions;

public class JWSSignatureException extends RuntimeException {
    public JWSSignatureException() {
        super();
    }

    public JWSSignatureException(String message) {
        super(message);
    }

    public JWSSignatureException(String message, Throwable cause) {
        super(message, cause);
    }

    public JWSSignatureException(Throwable cause) {
        super(cause);
    }

    protected JWSSignatureException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
