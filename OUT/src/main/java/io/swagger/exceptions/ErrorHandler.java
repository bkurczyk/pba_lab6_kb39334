package io.swagger.exceptions;

import io.jsonwebtoken.ExpiredJwtException;
import io.swagger.exceptions.CitizenshipParseException;
import io.swagger.exceptions.ClientAlreadyExistsException;
import io.swagger.exceptions.ClientDoesntExistsException;
import io.swagger.exceptions.UserNotFound;
import io.swagger.model.Error;
import io.swagger.model.ResponseHeader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.Date;
import java.util.UUID;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Error> handleRuntimeException(RuntimeException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("500");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<Error> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("400");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ExpiredJwtException.class)
    public ResponseEntity<Error> handleExpiredJwtException(ExpiredJwtException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("401");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(JwtUserException.class)
    public ResponseEntity<Error> handleJwtUserException(JwtUserException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("401");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(BadUsernameException.class)
    public ResponseEntity<Error> handleBadUsernameException(BadUsernameException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("401");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(BadPasswordExcepiton.class)
    public ResponseEntity<Error> handleBadPasswordExcepiton(BadPasswordExcepiton ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("401");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<Error> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("400");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ClientAlreadyExistsException.class)
    public ResponseEntity<Error> handleClientAlreadyExistsException(ClientAlreadyExistsException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("USER_ALREADY_EXISTS");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(ClientDoesntExistsException.class)
    public ResponseEntity<Error> handleClientDoesntExistsException(ClientDoesntExistsException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("USER_DOESNT_EXISTS");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(UserNotFound.class)
    public ResponseEntity<Error> handleUserNotFound(UserNotFound ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("USER_NOT_FOUND");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(CitizenshipParseException.class)
    public ResponseEntity<Error> handleCitizenshipParseException(CitizenshipParseException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("400");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.BAD_REQUEST);
    }
}
