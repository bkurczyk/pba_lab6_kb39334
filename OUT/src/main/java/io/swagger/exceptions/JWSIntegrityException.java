package io.swagger.exceptions;

public class JWSIntegrityException extends RuntimeException {
    public JWSIntegrityException() {
        super();
    }

    public JWSIntegrityException(String message) {
        super(message);
    }

    public JWSIntegrityException(String message, Throwable cause) {
        super(message, cause);
    }

    public JWSIntegrityException(Throwable cause) {
        super(cause);
    }

    protected JWSIntegrityException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
