package io.swagger.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.ByteStreams;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import io.jsonwebtoken.*;
import io.swagger.annotations.ApiParam;
import io.swagger.exceptions.*;
import io.swagger.model.*;
import io.swagger.model.db.UserDB;
import io.swagger.repo.UserRepo;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestHeader;
import sun.security.x509.X509CertImpl;

import javax.annotation.PostConstruct;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.Certificate;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-11-03T14:30:00.307+01:00")

@Controller
public class UsersApiController implements UsersApi {
    private UserRepo userRepo;
    private byte[] sharedKey = new byte[32];

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final String userOAuth2 = "pba_user";
    private final String userBasicAuth = "pba_user";
    private final String userBasicPasswd = "123456";

    byte[] secretKey = "123456".getBytes();

    private String publicKey = "-----BEGIN PUBLIC KEY-----" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxBXKflInOIinBr2Y0rEB" +
            "9VkDp/+bzXMB0cPr94wLUsV2uY8wI0j2JormJmU2JlNWiIgnJFVNBzFhvOTT6eeR" +
            "L72+eKHWCiZz2FKEcjTTPt15Bdj78He5WLz3B5ezCvZMpYHbM8+NQ1wCkQt3G/ZL" +
            "txZ71hj60V0/RxXP1ltFScZ3IGFzNpI1nw11foQTzeOxmgYu0iuqIQXYBa6IpbBR" +
            "2VKcKXa9a4M4e2jB6FisfnEae701KhmjHMPo3pUwzRtrcSegBTmtECQaW66yJibX" +
            "saErLz4R0lElyjAg8uD4IZLrni26V12HqLerVY3guRm5U2s9U2HBDKVohD/fTTJ0" +
            "MwIDAQAB" +
            "-----END PUBLIC KEY-----";

    @Autowired
    public UsersApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @PostConstruct
    public void init() {
        List<UserDB> listOfUsers = new ArrayList<UserDB>();
        UserDB user = UserDB.builder().id(UUID.fromString("1c0f1731-d827-4f84-a19e-31875fb5fe71")).name("John").surname("Smith").age(24).personalId("97070808569")
                .citizenship("PL").email("js@gmail.com").build();
        listOfUsers.add(user);
        user = UserDB.builder().id(UUID.fromString("ac455cac-7245-44fd-8d50-8223fbced82b")).name("Eric").surname("Johnson").age(23).personalId("98080908123")
                .citizenship("DE").email("ej@gmail.com").build();
        listOfUsers.add(user);
        user = UserDB.builder().id(UUID.fromString("d9319637-5d38-4979-be4e-be5dcea6128a")).name("Nick").surname("Brown").age(25).personalId("96010108987")
                .citizenship("UK").email("nb@gmail.com").build();
        listOfUsers.add(user);
        userRepo = new UserRepo(listOfUsers);

        new SecureRandom().nextBytes(sharedKey);
        System.out.println(sharedKey);
    }

    @RequestMapping(value = "/jws",
            produces = { "application/json" },
            method = RequestMethod.GET)
    public ResponseEntity<String> getJWS(@Valid @RequestBody String body) throws JOSEException {
        return ResponseEntity.ok().body(jws(body));
    }

    @Override
    public Optional<ObjectMapper> getObjectMapper() {
        return Optional.ofNullable(objectMapper);
    }

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<UserListResponse> getAllUsers(@RequestHeader("Authorization") String token) {
        BasicAuth(token);
        List<User> Users = userRepo.getListOfUsers().stream()
                .map(p -> new User(p.getId(), p.getName(), p.getSurname(), p.getAge(), p.getPersonalId(), User.CitizenshipEnum.valueOf(p.getCitizenship()), p.getEmail()))
                .collect(Collectors.toList());
        return ResponseEntity.ok().body(new UserListResponse().usersList(Users).responseHeader(new ResponseHeader().requestId(UUID.randomUUID()).sendDate(new Date(System.currentTimeMillis()))));
    }

    @Override
    public ResponseEntity<UserResponse> getUserById(@ApiParam(value = "", required = true) @PathVariable("id") UUID id, @RequestHeader("Authorization") String token) {
        BasicAuth(token);
        User user = userRepo.getListOfUsers().stream().filter(u -> u.getId().equals(id)).findFirst()
                .map(u -> new User(u.getId(), u.getName(), u.getSurname(), u.getAge(), u.getPersonalId(), User.CitizenshipEnum.valueOf(u.getCitizenship()), u.getEmail())).orElse(null);
        if (user == null) throw new UserNotFound("User with this Id not found");
        return ResponseEntity.ok().body(new UserResponse().user(user).responseHeader(new ResponseHeader().requestId(UUID.randomUUID()).sendDate(new Date(System.currentTimeMillis()))));
    }

    @Override
    public ResponseEntity<UserResponse> createUser(@ApiParam(value = "User object that has to be added", required = true) @Valid @RequestBody String body, @RequestHeader("Authorization") String token, @RequestHeader("X-HMAC-SIGNATURE") String hmacSignature) throws JsonProcessingException, JOSEException, NoSuchAlgorithmException, InvalidKeyException {
        BasicAuth(token);
        hmacCheck(body, hmacSignature);
        CreateRequest req = objectMapper.readValue(body, CreateRequest.class);
        User user = req.getUser();
        ValidateCitizenship(user.getCitizenship());
        userRepo.addNewUser(new UserDB(user.getId(), user.getName(), user.getSurname(), user.getAge(), user.getPersonalId(), user.getCitizenship().toString(), user.getEmail()));

        return ResponseEntity.ok().body(new UserResponse().user(user).responseHeader(new ResponseHeader().requestId(req.getRequestHeader().getRequestId()).sendDate(new Date(System.currentTimeMillis()))));

    }

    private void hmacCheck(String body, String hmacSignature) throws NoSuchAlgorithmException, InvalidKeyException {
        byte[] hmacSha256 = null;
        Mac mac = Mac.getInstance("HmacSHA256");
        SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey, "HmacSHA256");
        mac.init(secretKeySpec);
        hmacSha256 = mac.doFinal(body.getBytes());
        String hmacSha256Hex = String.format("%032x", new BigInteger(1, hmacSha256));
        if (!hmacSignature.equals(hmacSha256Hex))
        {
            throw new HMACIntegrityException("Body of request has been changed");
        }
    }

    @Override
    public ResponseEntity<Void> deleteUser(UUID id, @RequestHeader("Authorization") String token) throws Exception {
        OAuth2(token);
        userRepo.deleteUserById(id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<UserResponse> updateUser(UUID id, @Valid String body, @RequestHeader("Authorization") String token, @RequestHeader("X-JWS-SIGNATURE") String jwsSignature) throws Exception {
        //OAuth2(token);
        JWSCheck(body,jwsSignature);
        CreateRequest req = objectMapper.readValue(body, CreateRequest.class);
        User user = req.getUser();
        ValidateCitizenship(user.getCitizenship());
        userRepo.updateUserById(id, new UserDB(id, user.getName(), user.getSurname(), user.getAge(), user.getPersonalId(), user.getCitizenship().toString(), user.getEmail()));
        return ResponseEntity.ok().body(new UserResponse().user(user).responseHeader(new ResponseHeader().requestId(req.getRequestHeader().getRequestId()).sendDate(new Date(System.currentTimeMillis()))));
    }

    private void JWSCheck(String body, String jwsSignature) throws JOSEException, ParseException {
        JWSObject jwsObject = JWSObject.parse(jwsSignature);
        JWSVerifier verifier = new MACVerifier(sharedKey);
        if(!jwsObject.verify(verifier)){
            throw new JWSSignatureException("JWS was created with different private key");
        }
        String jwsFromBody = jws(body);
        if(!jwsFromBody.equals(jwsSignature)) {
            throw new JWSIntegrityException("Body of request has been changed");
        }
    }

    private void ValidateCitizenship(User.CitizenshipEnum citizenship) {
        if (citizenship == null)
            throw new CitizenshipParseException("Can't parse Citizenship");
    }

    public static RSAPublicKey readPublicKey(String key) throws Exception {
        String publicKeyPEM = key.replace("-----BEGIN PUBLIC KEY-----", "").replaceAll(System.lineSeparator(), "").replace("-----END PUBLIC KEY-----", "");
        byte[] encoded = Base64.getDecoder().decode(publicKeyPEM);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
        return (RSAPublicKey) keyFactory.generatePublic(keySpec);
    }

    private void OAuth2(String token) throws Exception {
        PublicKey pKey = readPublicKey(publicKey);
        String tokenAfterReplace = token.replace("Bearer ", "");

        JwtParser parser = Jwts.parser().setSigningKey(pKey);
        Jws<Claims> claims = parser.parseClaimsJws(tokenAfterReplace);

        String userFromClaims = claims.getBody().get("client_id",String.class);

        if (!userFromClaims.equals(userOAuth2)){
            throw new JwtUserException("Jwt user is wrong");
        }

    }
    private void BasicAuth(String token) {
        String tokenAfterReplace = token.replace("Basic ", "");

        byte[] decoded = Base64.getDecoder().decode(tokenAfterReplace);
        String decodedString = new String(decoded, StandardCharsets.UTF_8);
        String[] credentials = decodedString.split(":");

        if(!credentials[0].equals(userBasicAuth)) throw new BadUsernameException("Wrong username");
        if(!credentials[1].equals(userBasicPasswd)) throw  new BadPasswordExcepiton("Wrong password");
    }

    private String jws(String json) throws JOSEException {
        JWSObject jwsObject = new JWSObject(new JWSHeader(JWSAlgorithm.HS256),
                new Payload(json));

        jwsObject.sign(new MACSigner(sharedKey));
        return jwsObject.serialize();
    }
}
