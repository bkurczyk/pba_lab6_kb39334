package io.swagger.repo;

import io.swagger.exceptions.ClientAlreadyExistsException;
import io.swagger.exceptions.ClientDoesntExistsException;
import io.swagger.model.db.UserDB;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
@NoArgsConstructor
@Getter
public class UserRepo {
    private List<UserDB> listOfUsers;


    public UserRepo(List<UserDB> listOfUsers) {

        this.listOfUsers = new ArrayList<UserDB>();
        this.listOfUsers = listOfUsers;
    }
    public void addNewUser(UserDB newUser){
        if(this.listOfUsers.stream().filter(u->u.getId().equals(newUser.getId()) || u.getPersonalId().equals(newUser.getPersonalId())).findAny().orElse(null) == null){
            this.listOfUsers.add(newUser);
        }else {
            StringBuilder stringBuilder = new StringBuilder();
            if (!(this.listOfUsers.stream().filter(u->u.getId().equals(newUser.getId())).findAny().orElse(null)==null))
            {
                stringBuilder.append("User with this Id already exists. ");
            }
            if(!(this.listOfUsers.stream().filter(u->u.getPersonalId().equals(newUser.getPersonalId())).findAny().orElse(null)==null))
                stringBuilder.append("User with this PersonalId already exists.");
            throw new ClientAlreadyExistsException(stringBuilder.toString());
        }

    }
    public void deleteUserById(UUID id)
    {
        UserDB foundUser = this.listOfUsers.stream().filter(x-> x.getId().equals(id)).findFirst().orElse(null);
        if(foundUser!=null)
        {
            this.listOfUsers.remove(foundUser);
        }else{
            throw new ClientDoesntExistsException("User with this Id doesn't exists");
        }
    }

    public void updateUserById(UUID id, UserDB updatedUser)
    {
        UserDB foundUser = this.listOfUsers.stream().filter(x-> x.getId().equals(id)).findFirst().orElse(null);
        UserDB foundWithTheSamePersonalId = this.listOfUsers.stream().filter(x-> x.getPersonalId().equals(updatedUser.getPersonalId()) && !x.getId().equals(id)).findAny().orElse(null);
        if(foundWithTheSamePersonalId!=null) throw new ClientAlreadyExistsException("User with this PersonalId already exists.");
        if(foundUser!=null)
        {
            this.listOfUsers.set(this.listOfUsers.indexOf(foundUser),updatedUser);
        }else{
            throw new ClientDoesntExistsException("User with this Id doesn't exists");
        }
    }
}
